const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const schema = mongoose.Schema;

const userSchema = new schema({
    username:{type:String,required:true},
    name:{type:String,required:true},
    email:{type:String,require:true},
    password:{type:String,required:true}

});

module.exports = mongoose.model("User",userSchema);

 module.exports.saveUser = function (newUser,callback) {
     bcrypt.genSalt(10, function(err, salt) {
         bcrypt.hash(newUser.password, salt, function(err, hash) {
        newUser.password = hash;
        
        if (err) throw err;
        newUser.save(callback);
         });
     });

 };